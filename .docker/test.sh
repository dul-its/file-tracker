#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

export RAILS_ENV=test

docker compose \
    -p "file-tracker-${CI_COMMIT_SHORT_SHA:-test}" \
    -f docker-compose.yml \
    -f docker-compose.test.yml \
    "$@"
