#!/bin/bash

context="$(git rev-parse --show-toplevel)"

cd "$(dirname ${BASH_SOURCE[0]})"

./test.sh -f docker-compose.test-interactive.yml up -d
./test.sh -f docker-compose.test-interactive.yml exec app bash
./test.sh -f docker-compose.test-interactive.yml down
