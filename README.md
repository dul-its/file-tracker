# file-tracker

A Rails application for tracking files in directories.

## Requirements

- Ruby 2.7
- PostgreSQL
- GNU Make

## Configuration

See `lib/file_tracker.rb`.

### i18n

See `config/locales/en.yml` for i18n keys.

## Inventory

The basic tracking process ("inventory") crawls the tracked directories,
queueing up background jobs for each subdirectory and file.

To run the inventory process, execute the task

    $ rake file_tracker:inventory[:id]

The `[:id]` argument optionally specifies an individual directory to inventory, as opposed to all directories.

## Fixity checking

To run a fixity check for files that are due to be (re-)checked, run:

    $ rake file_tracker:fixity[:max]

The `[:max]` argument is optional and, if present, overrides `BATCH_FIXITY_CHECK_LIMIT`.

## Build

To build the application container, run:

    $ make

## Development/Test

To bring up a development environment with local code mounted in the app container, run

    $ .docker/dev.sh up

## API

See [docs/API.md](docs/API.md).
