class BatchTouchService
  def self.call(ids)
    TrackedFile.where(id: ids).touch_all
  end
end
