require 'fileutils'

class UpdatedbService
  def self.call(tracked_directory)
    FileUtils.mkdir_p FileTracker.updatedb_dir
    db = UpdatedbPath.call(tracked_directory)
    cmd = "updatedb -l 0 -o #{db} -U #{tracked_directory.path}"
    pid = spawn(cmd)
    Process.wait(pid)
    $?.exitstatus
  end
end
