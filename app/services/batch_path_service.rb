class BatchPathService
  def self.call(paths)
    new(paths).run
  end

  attr_reader :paths

  def initialize(paths)
    @paths = paths
  end

  def run
    raise NotImplementedError
  end
end
