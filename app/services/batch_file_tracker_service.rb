class BatchFileTrackerService < BatchPathService
  def tracked_files
    @tracked_files ||= TrackedFile.where(path: paths).to_a
  end

  def new_paths
    @new_paths ||= (paths - old_paths).select { |path| FileTest.file?(path) }
  end

  def old_paths
    @old_paths ||= tracked_files.map(&:path)
  end

  def run
    process_new_paths if new_paths.present?
    process_old_paths if old_paths.present?
  end

  private

  def check_paths
    @check_paths ||= []
  end

  def touch_ids
    @touch_ids ||= []
  end

  def process_new_paths
    new_records = new_paths.map do |path|
      # Guards somewhat against a possible race condition,
      # i.e., a delete/move between the time the new path
      # was discovered and getting the file size here.

      { path:, size: File.size(path) }
    rescue Exception => e
      Rails.logger.error { e }
      nil
    end.compact

    BatchInsertService.call(new_records)

    BatchFixityJob.perform_later(new_paths)
  end

  def process_old_paths
    tracked_files.each do |tracked_file|
      process_tracked_file(tracked_file)
    end

    BatchFixityJob.perform_later(check_paths) if check_paths.present?

    return unless touch_ids.present?

    BatchTouchService.call(touch_ids)
  end

  def process_tracked_file(tracked_file)
    size = begin
      File.size(tracked_file.path)
    rescue StandardError
      nil
    end

    if size.nil?
      FileMovedOrRemovedCommand.call(tracked_file)
    elsif size != tracked_file.size
      check_paths << tracked_file.path
    else
      touch_ids << tracked_file.id
    end
  end
end
