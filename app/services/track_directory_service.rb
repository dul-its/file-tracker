class TrackDirectoryService
  def self.call(tracked_directory)
    locator = LocateService.new(tracked_directory)

    locator.each_slice(FileTracker.track_files_batch_size) do |paths|
      BatchFileTrackerJob.perform_later(paths)
    end

    tracked_directory.update!(tracked_at: DateTime.now)
  end
end
