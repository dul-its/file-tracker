#
# Inserts new records into the database in bulk.
#
# N.B. This class is not responsible for setting full fixity info.
# It also uses insert_all, which bypasses Rails validation, and
# silently skips records that violate a uniqueness constraint in
# the database.
#
# https://api.rubyonrails.org/classes/ActiveRecord/Persistence/ClassMethods.html#method-i-insert_all
#
class BatchInsertService
  def self.call(*args)
    new(*args).run
  end

  attr_reader :records

  def initialize(records)
    #
    # In Rails >= 7.0, we can use `record_timestamps: true' in the
    # `insert_all' call and we don't need this.
    #
    now = DateTime.now

    @records = records.map do |record|
      { created_at: now, updated_at: now }.merge(record)
    end
  end

  def run
    return if records.empty?

    result = TrackedFile.insert_all(records, unique_by: :path)

    # It would be ideal to log here ...
    ids = result.to_a.map { |r| r['id'] }

    # ... instead of after the query here.
    TrackedFile.find(ids).each { |tf| LogAddedCommand.call(tf) }

    ids
  end
end
