require 'shellwords'

class BatchPathDigestService < BatchPathService
  # @return [Hash<String, Hash>] hash mapping paths to fixity attributes
  def run
    paths.index_with { |_path| nil }.tap do |value|
      results = HashdeepCommand.call(paths)

      checked_at = DateTime.now

      results.lines("\u0000", chomp: true).each do |line|
        size, sha1, path = line.split(',', 3)

        value[path] = { size: size.to_i, sha1:, fixity_checked_at: checked_at }
      end
    end
  rescue Errno::E2BIG => e # "Argument list too long"
    Rails.logger.warn { e }

    split_and_merge
  end

  def split_and_merge
    at = paths.size / 2
    paths_1 = paths[0, at]
    paths_2 = paths[at..-1]

    run_1 = BatchPathDigestService.call(paths_1)
    run_2 = BatchPathDigestService.call(paths_2)

    run_1.merge(run_2)
  end
end
