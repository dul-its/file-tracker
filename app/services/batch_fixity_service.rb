class BatchFixityService < BatchPathService
  def run
    results = BatchPathDigestService.call(paths)

    results.each do |path, attributes|
      if attributes.nil?
        RemovePathCommand.call(path)
      else
        AddOrUpdateFileCommand.call(path, attributes)
      end
    end
  end
end
