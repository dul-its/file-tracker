class FixityService
  attr_accessor :limit, :batch_size

  def self.call(limit: nil, batch_size: nil)
    new(limit:, batch_size:).run
  end

  def initialize(limit: nil, batch_size: nil)
    @limit = limit ? limit.to_i : FileTracker.batch_fixity_check_limit
    @batch_size = batch_size ? batch_size.to_i : FileTracker.fixity_check_batch_size
  end

  def run
    paths.each_slice(batch_size) do |batch|
      BatchFixityJob.perform_later(batch)
    end
  end

  def tracked_files
    TrackedFile
      .where('size > 0 AND (fixity_checked_at IS NULL OR updated_at < ? OR (fixity_checked_at < ? AND size <= ?))',
             check_last_seen_date, fixity_check_cutoff_date, FileTracker.large_file_threshold)
      .order(fixity_checked_at: :asc, created_at: :asc)
      .limit(limit)
  end

  def paths
    tracked_files.pluck(:path)
  end

  def fixity_check_cutoff_date
    DateTime.now - FileTracker.fixity_check_period.days
  end

  def check_last_seen_date
    DateTime.now - FileTracker.check_last_seen_period.days
  end
end
