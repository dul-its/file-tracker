#
# Finds matching file records based on SHA1.
#
class FindMatchingFilesService
  def self.call(tracked_file)
    return [] if tracked_file.empty?
    return [] unless tracked_file.sha1?

    TrackedFile
      .where(sha1: tracked_file.sha1)
      .where.not(path: tracked_file.path)
      .select(&:exist?)
  end
end
