class LocateService
  include Enumerable

  attr_reader :tracked_directory

  def initialize(tracked_directory)
    @tracked_directory = tracked_directory
  end

  def stats
    IO.popen("locate -d #{db} -c \\*") { |io| io.read }
  end

  def each
    IO.popen("locate -d #{db} -0 \\*") do |io|
      while path = io.gets("\u0000", chomp: true)
        yield path
      end
    end
  end

  private

  def db
    UpdatedbPath.call(tracked_directory)
  end
end
