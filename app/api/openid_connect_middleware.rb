class OpenidConnectMiddleware < Rack::Auth::AbstractHandler
  def call(env)
    # We already have an auth'd user, possibly from session
    return @app.call(env) if env['warden'].user

    auth = OpenidConnectMiddleware::Request.new(env)

    return redirect unless auth.provided?

    return bad_request unless auth.bearer?

    #
    # auth.params is the token string itself,
    # not including the 'Bearer' prefix.
    #
    # See Rack::Auth::AbstractRequest#params
    #
    if user = @authenticator.call(auth.params)
      env['warden'].set_user(user)

      return @app.call(env)
    end

    unauthorized
  end

  private

  def redirect
    [302,
     { 'Location' => '/users/auth/openid_connect' },
     []]
  end

  def challenge
    'Bearer realm="FileTracker API"'
  end

  class Request < Rack::Auth::AbstractRequest
    def bearer?
      scheme == 'bearer' && params.present?
    end
  end
end
