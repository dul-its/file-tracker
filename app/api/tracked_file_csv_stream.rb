require 'csv'

class TrackedFileCSVStream
  attr_reader :relation, :fields

  def initialize(relation, fields)
    @relation = relation
    @fields = fields
  end

  def headers
    if fields.include?('ddr_resources')
      # To add DDR id if there is only one match ...
      fields + ['ddr_id']
    else
      fields
    end
  end

  def row(tracked_file)
    tf = TrackedFileEntity.represent(tracked_file, only: fields)
    tf_hash = tf.serializable_hash.stringify_keys

    if fields.include?('ddr_resources')
      count = tf.ddr_resources.length

      # Use 'ddr_resources' col for count
      tf_hash['ddr_resources'] = count
      # Add DDR id if there is only one match
      tf_hash['ddr_id'] = count == 1 ? tf.ddr_resources.first['id'] : nil
    end

    tf_hash.values_at(*headers)
  end

  def each
    @headers_yielded = false

    relation.find_each do |tracked_file|
      unless @headers_yielded
        yield CSV.generate_line(headers, **csv_options)

        @headers_yielded = true
      end

      yield CSV.generate_line(row(tracked_file), **csv_options)
    end
  end

  def csv_options
    { encoding: Encoding::UTF_8 }
  end
end
