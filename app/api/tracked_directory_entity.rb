class TrackedDirectoryEntity < Entity
  expose :id,
         documentation: { type: 'integer' }

  expose :path

  expose :title

  expose :tracked_at,
         documentation: { type: 'string', format: 'date-time' }

  expose :created_at,
         documentation: { type: 'string', format: 'date-time' }

  expose :updated_at,
         documentation: { type: 'string', format: 'date-time' }
end
