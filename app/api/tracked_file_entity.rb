class TrackedFileEntity < Entity
  expose :id,
         documentation: { type: 'integer' }

  expose :path

  expose :sha1

  expose :size,
         documentation: { type: 'integer' }

  expose :fixity_checked_at,
         documentation: { type: 'string', format: 'date-time' }

  expose :created_at,
         documentation: { type: 'string', format: 'date-time' }

  expose :updated_at,
         documentation: { type: 'string', format: 'date-time' }

  expose :ddr_resources, using: DdrResourceEntity, if: { ddr_resources: true },
                         documentation: { is_array: true }


  def ddr_resources
    return [] unless object.sha1?

    Rails.cache.fetch("ddr-resources/#{object.sha1}", expires_in: 24.hours) do
      response = Net::HTTP.get_response(
        URI("https://ddr-admin.lib.duke.edu/api/resources?sha1=#{object.sha1}"),
        {'accept'=>'application/json', 'authorization'=>"Bearer #{ENV['DDR_CLIENT_KEY']}"}
      )

      raise FileTrackerAPI::Error, response.message if response.code != '200'

      JSON.parse(response.body)
    end
  end
end
