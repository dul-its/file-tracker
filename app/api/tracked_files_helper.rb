module TrackedFilesHelper
  extend Grape::API::Helpers

  include Pagy::Backend

  params :pagination do
    optional Pagy::DEFAULT[:page_param], # i.e, :page
             type: Integer,
             desc: 'Page number of results (JSON only; default: %s)' % Pagy::DEFAULT[:page]

    optional :per_page,
             type: Integer,
             desc: 'Number (max) of records per page (JSON only; default %s)' % Pagy::DEFAULT[:items]
  end

  def add_page_headers
    return unless @pagy

    pagy_headers(@pagy).each do |key, value|
      header key, value
    end
  end
end
