module APIConstants
  TITLE   = 'Duke Libraries FileTracker API'
  VERSION = 'v1.beta'

  CSV_MEDIA  = 'text/csv'
  JSON_MEDIA = 'application/json'
  TEXT_MEDIA = 'text/plain'

  # Success responses
  OK       = { code: 200, message: 'OK' }.freeze
  CREATED  = { code: 201, message: 'Created' }.freeze
  ACCEPTED = { code: 202, message: 'Accepted' }.freeze

  # Error responses
  BAD_REQUEST  = [400, 'Bad Request'].freeze
  UNAUTHORIZED = [401, 'Unauthorized'].freeze
  FORBIDDEN    = [403, 'Forbidden'].freeze
  NOT_FOUND    = [404, 'Not Found'].freeze
  CONFLICT     = [409, 'Conflict'].freeze
  SERVER_ERROR = [500, 'Server Error'].freeze
end
