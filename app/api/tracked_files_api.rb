class TrackedFilesAPI < Grape::API
  include APIConstants
  include OpenidConnectAuthentication

  authenticate_user

  helpers TrackedFilesHelper

  content_type :json, JSON_MEDIA
  content_type :csv, CSV_MEDIA

  default_format :json

  # GET /tracked_files
  desc 'Retrieve file records' do
    produces [JSON_MEDIA, CSV_MEDIA]
    failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED, FORBIDDEN]
    success TrackedFileEntity
    is_array true
  end
  params do
    optional :ddr_resources,
             type: Boolean,
             desc: 'Include matching DDR resources by SHA1',
             default: false

    optional :folder,
             type: String,
             desc: 'Limits results to file under the folder path',
             coerce_with: ->(val) { val.end_with?('/') ? val : val + '/' },
             allow_blank: false

    optional :sha1,
             desc: 'Match one or more SHA1 digests',
             type: [String],
             coerce_with: ->(val) { val.split(',') },
             allow_blank: false,
             documentation: {
               collectionFormat: 'csv',
               param_type: 'query'
             }

    optional :size,
             type: Integer,
             desc: 'Filter by exact file size (in bytes)',
             allow_blank: false

    optional :minsize,
             type: Integer,
             desc: 'Filter by minimum file size (in bytes)',
             allow_blank: false

    optional :maxsize,
             type: Integer,
             desc: 'Filter by maximum file size (in bytes)',
             allow_blank: false

    optional :fields,
             desc: 'Limits fields returned (CSV format only)',
             type: [String],
             # I would like to use TrackedFile.attribute_names for values and default,
             # but that causes initialization hassles when building the
             # container image because it tries to touch the database.
             values: %w[id path sha1 size fixity_checked_at created_at updated_at],
             default: %w[id path sha1 size fixity_checked_at created_at updated_at],
             coerce_with: ->(val) { val.split(',') },
             allow_blank: false,
             documentation: {
               collectionFormat: 'csv',
               param_type: 'query'
             }

    mutually_exclusive :path, :folder
    mutually_exclusive :size, :minsize
    mutually_exclusive :size, :maxsize

    use :pagination
  end
  after { add_page_headers }
  get do
    relation = TrackedFile.all
    relation = relation.where(path: params[:path]) if params[:path]
    relation = relation.where('path LIKE ?', params[:folder] + '%') if params[:folder]
    relation = relation.where('size >= ?', params[:minsize]) if params[:minsize]
    relation = relation.where('size <= ?', params[:maxsize]) if params[:maxsize]
    relation = relation.where(size: params[:size]) if params[:size]
    relation = relation.where(sha1: params[:sha1]) if params[:sha1]

    if env['api.format'] == :csv
      filename = 'File-Tracker-%s.csv' % DateTime.now.strftime('%Y%m%d%H%M%S')
      header 'Content-Disposition', 'attachment; filename="%s"' % filename

      # Rack 2.x ETag middleware has some issue that affects streaming:
      # https://github.com/rack/rack/issues/1619#issuecomment-848460528
      # This is supposedly one workaround:
      header 'Last-Modified', Time.now.httpdate

      fields = params[:fields].dup
      fields << 'ddr_resources' if params[:ddr_resources]

      stream TrackedFileCSVStream.new(relation, fields)
    else
      @pagy, records = pagy(relation, items: params[:per_page])

      present records, with: TrackedFileEntity, ddr_resources: params[:ddr_resources]
    end
  end

  # POST /tracked_files
  desc 'Track one or more new files' do
    produces [JSON_MEDIA]
    consumes [JSON_MEDIA]
    failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED]
    success [ACCEPTED]
    is_array true
  end
  params do
    requires :files,
             desc: 'File(s)',
             type: [JSON] do
      requires :path,
               type: String,
               desc: 'File path'
      # TODO: - SHA1 and size
      # optional :sha1,
      #          type: String,
      #          desc: 'Provided SHA1 digest',
      #          allow_blank: false
      #
      # optional :size,
      #          type: Integer,
      #          desc: 'Provided file size',
      #          allow_blank: false
    end
  end
  post do
    paths = params[:files].map { |f| f[:path] }

    # Prevents tracking files in non-tracked directories.
    begin
      paths.each { |path| TrackedDirectory.for_path(path) }
    rescue ActiveRecord::RecordNotFound => e
      error!('Unable to create file(s) due to error: %s' % e.message, 400)
    end

    BatchFileTrackerJob.perform_later(paths)
  end

  route_param :id, type: Integer do
    after_validation do
      @tracked_file = TrackedFile.find(params[:id])
    end

    # GET /tracked_files/:id
    desc 'Retrieve file by ID' do
      produces [JSON_MEDIA]
      failure [SERVER_ERROR, NOT_FOUND, UNAUTHORIZED]
      success TrackedFileEntity
    end
    params do
      optional :ddr_resources,
               type: Boolean,
               desc: 'Include matching DDR resources by SHA1',
               default: false
    end
    get do
      present @tracked_file, with: TrackedFileEntity, ddr_resources: params[:ddr_resources]
    end
  end
end
