class DdrResourceEntity < Grape::Entity

  self.hash_access = :to_s

  METADATA      = %w[permanent_id ingestion_date].freeze
  CONTENT_ATTRS = %w[sha1 media_type original_filename].freeze

  expose :id, :model

  METADATA.each do |field|
    expose field do |obj, _opts|
      obj.dig('metadata', field)
    end
  end

  CONTENT_ATTRS.each do |field|
    expose field do |obj, _opts|
      obj.dig('files', 'content', field)
    end
  end
end
