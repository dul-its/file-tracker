class Entity < Grape::Entity
  include Rails.application.routes.url_helpers

  class_attribute :default_url_options

  self.default_url_options = {}.tap do |opts|
    opts[:host] = ENV.fetch('APPLICATION_HOSTNAME', 'localhost')

    if opts[:host] == 'localhost'
      opts[:port] = ENV.fetch('RAILS_PORT', nil)
    else
      opts[:protocol] = 'https'
    end
  end
end
