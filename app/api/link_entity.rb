class LinkEntity < Grape::Entity
  expose :href, documentation: { type: 'string', format: 'uri' }
  expose :rel, documentation: { type: 'string' }
end
