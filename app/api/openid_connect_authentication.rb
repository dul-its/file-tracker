module OpenidConnectAuthentication
  extend ActiveSupport::Concern

  Grape::Middleware::Auth::Strategies.add(:openid_connect, OpenidConnectMiddleware)

  class_methods do
    def authenticate_user
      auth :openid_connect do |token|
        uri = URI('https://oauth.oit.duke.edu/oidc/introspect')

        req = Net::HTTP::Post.new(uri)
        req.basic_auth ENV.fetch('FT_OAUTH_CLIENT_ID'), ENV.fetch('FT_OAUTH_CLIENT_SECRET')
        req['Accept'] = 'application/json'
        req.content_type = 'application/x-www-form-urlencoded'
        req.body = URI.encode_www_form(token:)

        response = Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
          http.request(req)
        end

        response.value # raises an exception if response code not 2XX

        value = JSON.parse(response.body)

        User.find_by(uid: value['sub']) if value['active']
      end
    end
  end
end
