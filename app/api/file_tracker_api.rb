class FileTrackerAPI < Grape::API
  include APIConstants

  # Some kind of "server error"
  class Error < ::StandardError; end

  Grape::ContentTypes.register(:csv, 'text/csv')

  rescue_from ActiveRecord::RecordInvalid do |e|
    error!(e.record.errors.full_messages.join('; '), 400)
  end

  rescue_from ActiveRecord::RecordNotFound do |_e|
    error!(*NOT_FOUND.reverse)
  end

  rescue_from Error do |e|
    error!(e.message, 500)
  end

  mount TrackedDirectoriesAPI => 'tracked_directories'
  mount TrackedFilesAPI => 'tracked_files'

  add_swagger_documentation(
    {
      doc_version: '0.2.0',
      info: {
        title: TITLE,
        version: VERSION
      }
    }
  )
end
