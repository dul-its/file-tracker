class TrackedDirectoriesAPI < Grape::API
  include APIConstants
  include OpenidConnectAuthentication

  authenticate_user

  content_type :json, JSON_MEDIA
  content_type :txt,  TEXT_MEDIA

  default_format :json

  # GET /tracked_directories
  desc 'List tracked directories' do
    produces [JSON_MEDIA]
    failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED]
    success TrackedDirectoryEntity
    is_array true
  end
  get do
    present TrackedDirectory.all, with: TrackedDirectoryEntity
  end

  # POST /tracked_directories
  desc 'Add a tracked directory' do
    produces [JSON_MEDIA]
    consumes [JSON_MEDIA]
    success TrackedDirectoryEntity
    failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED]
  end
  params do
    requires :path, type: String, desc: 'Directory path'
    requires :title, type: String, desc: 'Directory title or description'
  end
  post do
    dir = TrackedDirectory.create! declared(params) # raises ActiveRecord::RecordInvalid
    header 'Location', '/api/tracked_directories/%s' % dir.id
    present dir, with: TrackedDirectoryEntity
  end

  # POST /tracked_directories/inventory
  desc 'Inventory all directories' do
    produces [JSON_MEDIA]
    success [ACCEPTED]
    failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED]
  end
  post 'inventory' do
    status 202
    dirs = TrackedDirectory.all.to_a
    TrackDirectoryBatchJob.perform_later(*dirs)
  end

  route_param :id, type: Integer do
    after_validation do
      @tracked_directory = TrackedDirectory.find(params[:id])
    end

    # GET /tracked_directories/:id
    desc 'Tracked directory' do
      produces [JSON_MEDIA]
      failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED, NOT_FOUND]
      success TrackedDirectoryEntity
    end
    get do
      present @tracked_directory, with: TrackedDirectoryEntity
    end

    # GET /tracked_directories/:id/locate
    desc 'Current stats of locate DB for the tracked directory' do
      produces [TEXT_MEDIA]
      success [OK]
      failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED, NOT_FOUND]
    end
    get 'locate' do
      LocateService.new(@tracked_directory).stats
    end

    # POST /tracked_directories/:id/locate
    desc 'Update the locate DB for the directory' do
      produces [JSON_MEDIA]
      success [ACCEPTED]
      failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED, NOT_FOUND]
    end
    post 'locate' do
      status 202
      UpdatedbJob.perform_later(@tracked_directory)
    end

    # POST /tracked_directories/:id/inventory
    desc 'Trigger directory inventory' do
      produces [JSON_MEDIA]
      success [ACCEPTED]
      failure [SERVER_ERROR, BAD_REQUEST, UNAUTHORIZED, NOT_FOUND]
    end
    post 'inventory' do
      status 202
      TrackDirectoryBatchJob.perform_later(@tracked_directory)
    end
  end
end
