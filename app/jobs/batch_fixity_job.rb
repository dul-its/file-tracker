class BatchFixityJob < ApplicationJob
  queue_as :fixity

  def perform(paths)
    BatchFixityService.call(paths)
  end
end
