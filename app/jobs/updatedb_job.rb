class UpdatedbJob < ApplicationJob
  queue_as :directory

  def perform(tracked_directory)
    UpdatedbService.call(tracked_directory)
  end
end
