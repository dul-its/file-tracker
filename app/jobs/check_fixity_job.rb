class CheckFixityJob < ApplicationJob
  queue_as :fixity

  def perform(tracked_file)
    tracked_file.check_fixity!
  end
end
