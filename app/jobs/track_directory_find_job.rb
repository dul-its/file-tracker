require 'find'

class TrackDirectoryFindJob < TrackDirectoryJob
  def perform(path)
    Find.find(path) do |subpath|
      TrackFileJob.perform_later(subpath) if FileTest.file?(subpath) && !(FileTest.symlink?(subpath) || path == subpath)
    rescue *FileTracker.log_file_errors => e
      logger.error(e)
    end
  end
end
