class TrackFileJob < ApplicationJob
  queue_as :file

  def perform(path)
    TrackedFile.track!(path)
  end
end
