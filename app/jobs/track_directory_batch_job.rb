class TrackDirectoryBatchJob < TrackDirectoryJob
  def perform(*tracked_directories)
    tracked_directories.each do |tracked_directory|
      TrackDirectoryService.call(tracked_directory)
    end
  end
end
