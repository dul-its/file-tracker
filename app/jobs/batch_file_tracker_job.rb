class BatchFileTrackerJob < ApplicationJob
  queue_as :file

  def perform(paths)
    BatchFileTrackerService.call(paths)
  end
end
