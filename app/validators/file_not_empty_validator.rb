class FileNotEmptyValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless File.zero?(value)

    record.errors.add(attribute, 'is an empty file')
  end
end
