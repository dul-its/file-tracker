class ReadableValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if File.readable?(value)

    record.errors.add(attribute, 'is not readable by the file-tracker application')
  end
end
