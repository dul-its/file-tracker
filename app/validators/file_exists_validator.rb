class FileExistsValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if File.file?(value)
      record.errors.add(attribute, 'cannot be a symbolic link') if File.symlink?(value)
    else
      record.errors.add(attribute, 'does not exist or is not a file')
    end
  end
end
