class DirectoryExistsValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return if File.directory?(value)

    record.errors.add(attribute, 'does not exist or is not a directory')
  end
end
