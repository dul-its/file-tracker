require 'devise'

class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def openid_connect
    user = resource_class.from_omniauth(request.env['omniauth.auth'])
    set_flash_message :notice, :success, kind: 'OpenID Connect'
    sign_in_and_redirect user
  end
end
