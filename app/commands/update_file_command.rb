class UpdateFileCommand
  def self.call(tracked_file)
    raise ArgumentError, 'Tracked file record has not changed.' unless tracked_file.changed?

    if tracked_file.sha1_changed? && !tracked_file.sha1_was.nil?
      FileModifiedCommand.call(tracked_file)
    else
      tracked_file.save!
    end
  end
end
