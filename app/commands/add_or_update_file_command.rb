class AddOrUpdateFileCommand
  def self.call(path, attributes)
    raise ArgumentError, "`path' argument must be present." if path.blank?

    tracked_file = TrackedFile.find_or_initialize_by(path:)
    tracked_file.attributes = attributes

    if tracked_file.new_record?
      AddFileCommand.call(tracked_file)
    else
      UpdateFileCommand.call(tracked_file)
    end
  end
end
