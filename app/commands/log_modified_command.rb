class LogModifiedCommand
  def self.call(tracked_file, sha1_was)
    raise ArgumentError, 'Previous SHA1 is missing.' if sha1_was.nil?

    msg = I18n.t('file_tracker.log.message.modified') % sha1_was
    LogCommand.call(tag: :modified, tracked_file:, msg:)
  end
end
