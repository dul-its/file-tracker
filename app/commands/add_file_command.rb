class AddFileCommand
  def self.call(tracked_file)
    tracked_file.save!

    LogAddedCommand.call(tracked_file)
  end
end
