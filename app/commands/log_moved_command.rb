class LogMovedCommand
  def self.call(moved_from:, moved_to:)
    unless moved_from.destroyed?
      raise ArgumentError, "Tracked file `moved_from' is not destroyed: #{moved_from.inspect}"
    end

    msg = I18n.t('file_tracker.log.message.moved_to') % moved_to.path

    LogCommand.call(tag: :moved, tracked_file: moved_from, msg:)
  end
end
