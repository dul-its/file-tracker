class LogMovedOrRemovedCommand
  def self.call(tracked_file)
    matching_files = FindMatchingFilesService.call(tracked_file)

    if matching_files.length == 1
      LogMovedCommand.call(moved_from: tracked_file, moved_to: matching_files.first)
    else
      msg = nil
      msg = I18n.t('file_tracker.log.message.removed') % matching_files.length if matching_files.length > 1
      LogRemovedCommand.call(tracked_file, msg:)
    end
  end
end
