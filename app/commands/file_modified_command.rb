class FileModifiedCommand
  def self.call(tracked_file)
    unless tracked_file.sha1_changed? && !tracked_file.sha1_was.nil?
      raise ArgumentError, "Tracked file SHA1 has not changed (#{tracked_file.inspect})"
    end

    sha1_was = tracked_file.sha1_was

    tracked_file.save!

    LogModifiedCommand.call(tracked_file, sha1_was)
  end
end
