class LogCommand
  FORMAT = "%<date>s\t%<tag>s\t%<path>s\t%<size>s\t%<sha1>s\t%<msg>s\n"

  def self.call(tag:, tracked_file:, msg: nil)
    data = {
      date: DateTime.now.iso8601,
      tag: I18n.t("file_tracker.log.tag.#{tag}"),
      path: tracked_file.path,
      size: tracked_file.size || '-',
      sha1: tracked_file.sha1 || '-',
      msg: msg || '-'
    }

    TrackedFile.logger << (FORMAT % data)
  end
end
