class RemovePathCommand
  def self.call(path)
    return unless tracked_file = TrackedFile.find_by(path:)

    FileMovedOrRemovedCommand.call(tracked_file)
  end
end
