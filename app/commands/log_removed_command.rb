class LogRemovedCommand
  def self.call(tracked_file, msg: nil)
    raise ArgumentError, "Tracked file is not destroyed: #{tracked_file.inspect}" unless tracked_file.destroyed?

    LogCommand.call(tag: :removed, tracked_file:, msg:)
  end
end
