class FileMovedOrRemovedCommand
  def self.call(tracked_file)
    raise ArgumentError, "Tracked file path exists (#{tracked_file.inspect})" if tracked_file.exist?

    tracked_file.destroy

    LogMovedOrRemovedCommand.call(tracked_file)
  end
end
