class LogAddedCommand
  def self.call(tracked_file)
    LogCommand.call(tag: :added, tracked_file:)
  end
end
