require 'shellwords'

class HashdeepCommand
  def self.call(paths)
    escaped_paths = paths.map(&:shellescape).join(' ')

    command = "hashdeep -c sha1 -0 -s #{escaped_paths} | sed '/^[\\%\\#]/d'"

    IO.popen(command) { |io| io.read }
  end
end
