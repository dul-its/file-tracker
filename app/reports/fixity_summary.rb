class FixitySummary
  def self.call
    %w[ok modified missing error].index_with do |status|
      TrackedFile.send(status).count
    end
  end
end
