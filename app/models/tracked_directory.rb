class TrackedDirectory < ActiveRecord::Base
  include TrackedDirectoryAdmin

  before_validation :normalize_path!
  after_create :updatedb

  validates :path, presence: true, readable: true, uniqueness: true
  validates :title, presence: true, uniqueness: true

  def self.for_path(path)
    TrackedDirectory.find_by!('path = substr(?, 1, length(path))', path)
  end

  def to_s
    path
  end

  def tracked_files
    TrackedFile.under(path)
  end

  delegate :count, to: :tracked_files

  def size
    tracked_files.sum(:size)
  end

  def updatedb
    UpdatedbJob.perform_later(self)
  end

  private

  def normalize_path!
    self.path = File.realdirpath(path)
  rescue Errno::ENOENT => _e
    errors.add :path, :not_found, message: 'is not found'
  end
end
