class Ability
  include CanCan::Ability

  attr_reader :user

  def initialize(user = nil)
    @user = user || User.new

    can :read, :dashboard
    can :access, :rails_admin

    can :read, :all
    can :export, TrackedFile
    can :show_in_app, :all
  end
end
