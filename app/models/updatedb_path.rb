class UpdatedbPath
  def self.call(tracked_directory)
    File.join FileTracker.updatedb_dir, updatedb_file(tracked_directory)
  end

  def self.updatedb_file(tracked_directory)
    tracked_directory.title.downcase.gsub(/ /, '_')
  end
end
