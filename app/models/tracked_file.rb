class TrackedFile < ActiveRecord::Base
  include TrackedFileAdmin

  # N.B. The DB table has a unique index on `path', so no constraint in this model.
  validates :path, presence: true
  validates :path, file_exists: true, on: :create

  scope :large, -> { where('size >= ?', FileTracker.large_file_threshold) }

  def self.logger
    @logger ||= Logger.new(FileTracker.logdev, FileTracker.log_shift_age)
  end

  def self.track!(*paths)
    warn "[DEPRECATION] `TrackedFile.track!(*paths)' is deprecated; use `BatchFileTrackerService.call(paths)' instead."
    BatchFileTrackerService.call(paths)
  end

  def self.under(path)
    return all if path.blank? || path == '/'

    value = path.sub(%r{/\z}, '') # remove trailing slash
    where('path LIKE ?', "#{value}/%")
  end

  def to_s
    path
  end

  def tracked_directory
    TrackedDirectory.for_path(path)
  end

  def large?
    size? && size >= FileTracker.large_file_threshold
  end

  def exist?
    File.exist?(path)
  end

  def empty?
    size == 0
  end

  def removed?
    !exist?
  end
end
