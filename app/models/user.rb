class User < ApplicationRecord
  validates :uid, presence: true, uniqueness: true
  devise :database_authenticatable, :omniauthable, omniauth_providers: [:openid_connect]
  before_create :set_password, unless: :encrypted_password?

  def self.from_omniauth(auth)
    find_or_create_by!(uid: auth.uid)
  end

  private

  def set_password
    self.password = Devise.friendly_token
  end
end
