require 'file_tracker/constants'
require 'file_tracker/error'
require 'file_tracker/status'

module FileTracker
  VERSION = format('%s+%s', ENV.fetch('APP_VERSION'), ENV.fetch('APP_VCS_REF'))

  mattr_accessor :batch_fixity_check_limit do
    ENV.fetch('BATCH_FIXITY_CHECK_LIMIT', 100_000).to_i
  end

  mattr_accessor :check_last_seen_period do
    ENV.fetch('CHECK_LAST_SEEN_PERIOD', '3').to_i
  end

  mattr_accessor :fixity_check_period do
    ENV.fetch('FIXITY_CHECK_PERIOD', '90').to_i
  end

  mattr_accessor :large_file_threshold do
    ENV.fetch('LARGE_FILE_THRESHOLD', 1_000_000_000).to_i
  end

  mattr_accessor :log_dir do
    ENV.fetch('FILE_TRACKER_LOG_DIR', Rails.root.join('log'))
  end

  mattr_accessor :log_shift_age do
    ENV.fetch('FILE_TRACKER_LOG_SHIFT_AGE', 'weekly')
  end

  mattr_accessor :log_file_errors do
    [Errno::EINVAL, Errno::ENOENT, Errno::EACCES]
  end

  mattr_accessor :track_directory_job do
    ENV.fetch('TRACK_DIRECTORY_JOB', 'TrackDirectoryFindJob')
  end

  mattr_accessor :track_files_batch_size do
    ENV.fetch('TRACK_FILES_BATCH_SIZE', '1000').to_i
  end

  mattr_accessor :fixity_check_batch_size do
    ENV.fetch('FIXITY_CHECK_BATCH_SIZE', '20').to_i
  end

  mattr_accessor :updatedb_dir do
    ENV.fetch('FILE_TRACKER_UPDATEDB_DIR', Rails.root.join('tmp', 'updatedb'))
  end

  def self.logdev
    File.join(log_dir, "tracked-files.#{Rails.env}.log")
  end

  def self.config
    @config ||= class_variables(false).each_with_object({}) do |var, memo|
      memo[var.to_s.sub('@@', '').to_sym] = class_variable_get(var)
    end.slice(:batch_fixity_check_limit, :check_last_seen_period, :fixity_check_period, :large_file_threshold)
  end
end
