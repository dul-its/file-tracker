require 'file_tracker'

namespace :file_tracker do
  desc 'Update the locate database for all or one tracked directory.'
  task :updatedb, [:id] => :environment do |_t, args|
    if id = args[:id]
      td = TrackedDirectory.find(id)
      UpdatedbJob.perform_later(td)
    else
      TrackedDirectory.find_each do |td|
        UpdatedbJob.perform_later(td)
      end
    end
  end

  desc 'Inventory all tracked directories, or single directory by ID.'
  task :inventory, [:id] => :environment do |_t, args|
    if id = args[:id]
      tracked_directory = TrackedDirectory.find(id)
      TrackDirectoryBatchJob.perform_later(tracked_directory)
    else
      dirs = TrackedDirectory.all.to_a
      TrackDirectoryBatchJob.perform_later(*dirs)
    end
  end

  desc "Run the batch fixity check service, optionally overriding the default limit (#{FileTracker.batch_fixity_check_limit})."
  task :fixity, [:limit] => :environment do |_t, args|
    FixityService.call(limit: args[:limit])
  end
end
