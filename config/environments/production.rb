Rails.application.configure do
  config.action_controller.perform_caching = true
  config.action_mailer.perform_caching = false
  config.active_job.queue_adapter = :sidekiq
  config.active_record.dump_schema_after_migration = false
  config.active_support.report_deprecations = false
  config.assume_ssl = true
  config.cache_classes = true
  config.cache_store = :mem_cache_store if ENV['MEMCACHE_SERVERS'].present?
  config.consider_all_requests_local = false
  config.enable_reloading = false
  config.eager_load = true
  config.force_ssl = true
  config.i18n.fallbacks = true
  config.log_tags = [:request_id]
  config.public_file_server.enabled = true
  config.read_encrypted_secrets = false
end

RailsAdmin.config do |config|
  config.navigation_static_links['API']  = '/apidoc'
  config.navigation_static_links['Log']  = '/log'
end
