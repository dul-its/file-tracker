threads_count = ENV.fetch('RAILS_MAX_THREADS') { 5 }
threads threads_count, threads_count
workers ENV.fetch('WEB_CONCURRENCY') { 2 }
preload_app!
bind 'tcp://0.0.0.0:%s' % ENV.fetch('RAILS_PORT') { 3000 }
