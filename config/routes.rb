Rails.application.routes.draw do
  # Devise
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  # Rails admin
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  mount FileTrackerAPI => '/api'

  root to: 'rails_admin/main#dashboard'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/queues'
end
