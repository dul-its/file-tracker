Rails.autoloaders.each do |autoloader|
  autoloader.inflector.inflect(
    'file_tracker_api'=>'FileTrackerAPI',
    'tracked_directories_api'=>'TrackedDirectoriesAPI',
    'tracked_files_api'=>'TrackedFilesAPI',
    'tracked_file_csv_stream'=>'TrackedFileCSVStream',
  )
end
