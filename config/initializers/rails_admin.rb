RailsAdmin.config do |config|
  config.asset_source = :sprockets

  config.authenticate_with do
    warden.authenticate! scope: :user
  end

  config.current_user_method(&:current_user)

  config.authorize_with :cancancan

  config.included_models = %w[TrackedDirectory TrackedFile]

  config.actions do
    dashboard # mandatory
    index     # mandatory
    export
    show
    show_in_app
  end

  config.main_app_name = "File Tracker v#{FileTracker::VERSION}"

  config.navigation_static_links['Queues'] = '/queues'

  # Include empty fields on show views
  config.compact_show_view = false
end
