Devise.setup do |config|
  config.mailer_sender = 'no-reply@duke.edu'

  require 'devise/orm/active_record'

  config.authentication_keys = [:uid]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 11
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.sign_out_via = :get

  redirect_uri_base = if ENV['APPLICATION_HOSTNAME'].present?
                        'https://%s' % ENV['APPLICATION_HOSTNAME']
                      else
                        'http://localhost:3000'
                      end

  config.omniauth :openid_connect,
                  issuer: 'https://oauth.oit.duke.edu/oidc/',
                  discovery: true,
                  scope: %i[openid email profile],
                  response_type: :code,
                  client_options: {
                    scheme: 'https',
                    host: 'oauth.oit.duke.edu',
                    identifier: ENV.fetch('FT_OAUTH_CLIENT_ID', nil),
                    secret: ENV.fetch('FT_OAUTH_CLIENT_SECRET', nil),
                    redirect_uri: '%s/users/auth/openid_connect/callback' % redirect_uri_base
                  }

  # https://github.com/omniauth/omniauth/releases/tag/v2.0.0
  OmniAuth.config.allowed_request_methods = %i[get post]
  OmniAuth.config.silence_get_warning = true
end
