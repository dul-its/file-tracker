require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'action_cable/engine'
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module FileTracker
  class Application < Rails::Application
    config.load_defaults 7.2

    config.active_support.cache_format_version = 7.1

    config.colorize_logging = false

    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)

    config.log_level = ENV.fetch('RAILS_LOG_LEVEL', 'info').to_sym

    # Added in Rails 7.1 upgrade
    config.autoload_lib(ignore: %w(assets tasks))
    config.generators.system_tests = nil
  end
end

require 'file_tracker'
