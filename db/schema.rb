# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[6.1].define(version: 2022_06_01_193321) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "tracked_directories", force: :cascade do |t|
    t.string "path", null: false
    t.datetime "tracked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "title"
  end

  create_table "tracked_files", force: :cascade do |t|
    t.text "path", null: false
    t.string "sha1"
    t.bigint "size"
    t.datetime "fixity_checked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["created_at"], name: "index_tracked_files_on_created_at"
    t.index ["fixity_checked_at"], name: "index_tracked_files_on_fixity_checked_at"
    t.index ["path"], name: "index_tracked_files_on_path", unique: true
    t.index ["sha1"], name: "index_tracked_files_on_sha1"
    t.index ["size"], name: "index_tracked_files_on_size"
    t.index ["updated_at"], name: "index_tracked_files_on_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "uid", default: "", null: false
    t.string "email"
    t.string "encrypted_password", default: "", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_admin", default: false
  end

end
