class ChangeTrackedFilesPathIndex < ActiveRecord::Migration[6.1]
  def change
    remove_index :tracked_files, :path, if_exists: true
    add_index :tracked_files, :path, unique: true, if_not_exists: true
  end
end
