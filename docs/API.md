# FileTracker API

## Summary

The FileTracker API is mounted at **`/api`** by default on the server.

A **Swagger (OpenAPI) 2.0** document is served at `/api/swagger_doc.json`.
It details all the available methods, parameters, and responses.
The easiest way to use the Swagger doc is by the Swagger UI at `/apidoc` on the
servers; in the development environment, visit http://localhost:8080/apidoc.

## Authentication

API authentication requires a pre-existing FileTracker user.

### Browser-based

When using a browser, authentication is routed through the Duke NetID
single sign-on (SSO) service (via OpenID Connect/OAuth2, not Shibboleth, FWIW).

### Command-line, Script, etc.

To authenticate with CLI or script, each HTTP request must include an `Authorization`
header using the `Bearer` scheme and a valid Duke OAuth access token.

**Example**

    $ curl -H "Authorization: Bearer TOKEN" https://file-tracker.lib.duke.edu/api/tracked_files

where `TOKEN` is the access token string.

### Long-lived Access Tokens

The easiest way to create and use an access
token for this purpose is to a "long-lived" token at
[Duke OIT Self-Service/Advanced Options](https://idms-web-selfservice.oit.duke.edu/advanced)
under "Manage Your OAuth Secrets".  You can create a token for yourself or a managed non-personal
account (a.k.a. "service account") that corresponds to a FileTracker user account.

**Form options**

Duke UniqueID: Required - select account from list

Client ID Restriction: Optional - use `lib-file-tracker-oauth` to limit token usage to FT only

Endpoint Restriction Regex: N/A - leave blank
