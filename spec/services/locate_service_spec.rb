require 'rails_helper'

RSpec.describe LocateService do
  subject { described_class.new(tracked_directory) }

  let(:path) { Rails.root.join('spec/fixtures') }
  let(:tracked_directory) { TrackedDirectory.create!(path:, title: SecureRandom.hex(4)) }

  before do
    UpdatedbService.call(tracked_directory)
  end

  it 'locates files in the tracked directory' do
    expect(subject.to_a).not_to be_empty
  end
end
