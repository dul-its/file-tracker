require 'rails_helper'

RSpec.describe BatchFileTrackerService do
  let(:tracked_directory) { TrackedDirectory.create!(path: Rails.root.join('spec/fixtures'), title: SecureRandom.hex(4)) }
  let(:paths) { LocateService.new(tracked_directory).to_a }
  let(:files) { paths.select { |path| FileTest.file?(path) } }

  before do
    UpdatedbService.call(tracked_directory)
    TrackedFile.delete_all # FIXME - why is this necessary?!
    paths
  end

  it 'tracks new file paths' do
    expect { described_class.call(paths) }.to change { TrackedFile.count }.by(files.length)
  end

  it 'touches known paths' do
    tf = TrackedFile.create!(path: Rails.root.join('spec/fixtures/nypl.jpg'), size: 410_226)
    expect do
      described_class.call(paths)
      tf.reload
    end.to change(tf, :updated_at)
  end
end
