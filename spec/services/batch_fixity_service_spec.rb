require 'rails_helper'

RSpec.describe BatchFixityService do
  let(:tracked_directory) { TrackedDirectory.create!(path: Rails.root.join('spec/fixtures')) }
  let(:paths) { LocateService.new(tracked_directory).to_a }

  before do
    UpdatedbService.call(tracked_directory)
  end
end
