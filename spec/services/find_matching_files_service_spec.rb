require 'rails_helper'

RSpec.describe FindMatchingFilesService do
  let(:path1) do
    tmp = Tempfile.create('tf', encoding: 'ascii-8bit')
    tmp.write SecureRandom.random_bytes(100)
    tmp.close
    tmp.path
  end
  let(:sha1) { Digest::SHA1.file(path1).hexdigest }
  let!(:file1) do
    TrackedFile.create!(path: path1, sha1:)
  end
  let!(:file2) do
    TrackedFile.create!(path: path2, sha1:)
  end

  let(:path2) do
    tmp = Tempfile.create('tf', encoding: 'ascii-8bit')
    tmp.close
    FileUtils.cp(path1, tmp.path)
    tmp.path
  end

  after do
    File.unlink path1
    File.unlink path2
  end

  it 'returns the matching file records' do
    expect(described_class.call(file1)).to eq([file2])
  end
end
