require 'rails_helper'

RSpec.describe BatchPathDigestService do
  let(:tracked_directory) { TrackedDirectory.create!(path: Rails.root.join('spec/fixtures'), title: 'Test directory') }
  let(:paths) { LocateService.new(tracked_directory).to_a }

  before do
    UpdatedbService.call(tracked_directory)
  end

  it 'returns a Hash of paths to fixity attributes' do
    expect(described_class.call(paths).keys).to eq paths
  end

  it 'splits and merges when paths are too long' do
    service = described_class.new(paths)
    allow(HashdeepCommand).to receive(:call).and_call_original
    allow(HashdeepCommand).to receive(:call).with(paths).and_raise(Errno::E2BIG)
    allow(service).to receive(:split_and_merge).and_call_original
    expect(service.run.keys).to eq paths
    expect(service).to have_received(:split_and_merge)
  end
end
