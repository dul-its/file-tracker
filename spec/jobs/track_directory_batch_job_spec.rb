require 'rails_helper'

RSpec.describe TrackDirectoryBatchJob do
  describe '#perform' do
    before do
      @tracked_directory = TrackedDirectory.create!(path: Rails.root.join('spec/fixtures'), title: SecureRandom.hex(4))
    end

    it 'calls the TrackDirectoryService' do
      allow(TrackDirectoryService).to receive(:call)
      described_class.perform_now(@tracked_directory)
      expect(TrackDirectoryService).to have_received(:call)
    end
  end
end
