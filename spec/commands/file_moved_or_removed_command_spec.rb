require 'rails_helper'

RSpec.describe FileMovedOrRemovedCommand do
  it 'raises an error if the file exists' do
    file = TrackedFile.create!(path: Rails.root.join('spec/fixtures/nypl.jpg'))
    expect { described_class.call(file) }.to raise_error(ArgumentError)
  end

  it 'destroys the file record' do
    file = nil
    Tempfile.open('tf', encoding: 'ascii-8bit') do |tmp|
      tmp.write SecureRandom.random_bytes(100)
      tmp.close
      file = TrackedFile.create!(path: tmp.path)
      File.unlink(tmp.path)
    end

    expect { described_class.call(file) }.to change(file, :destroyed?).from(false).to(true)
  end

  it 'logs the result' do
    file = nil
    Tempfile.open('tf', encoding: 'ascii-8bit') do |tmp|
      tmp.write SecureRandom.random_bytes(100)
      tmp.close
      file = TrackedFile.create!(path: tmp.path)
      File.unlink(tmp.path)
    end

    allow(LogMovedOrRemovedCommand).to receive(:call).with(file)
    described_class.call(file)
    expect(LogMovedOrRemovedCommand).to have_received(:call).with(file)
  end
end
