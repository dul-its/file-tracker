require 'rails_helper'

RSpec.describe TrackedFile do
  let(:path) { Rails.root.join('spec/fixtures/nypl.jpg') }
  let(:sha1) { '37781031df4573b90ef045889b7da0ab2655bf74' }
  let(:size) { 410_226 }

  describe 'tracked_directory' do
    subject { described_class.new(path:) }

    let!(:dir) { TrackedDirectory.create!(path: Rails.root.join('spec/fixtures'), title: SecureRandom.hex(4)) }

    its(:tracked_directory) { is_expected.to eq dir }
  end

  describe 'validation' do
    subject { described_class.new(path:) }

    let(:file) { Tempfile.create('foo') }
    let(:path) { file.path }

    before do
      File.binwrite(path, SecureRandom.random_bytes(100))
    end

    after { File.unlink(path) if File.exist?(path) }

    it { is_expected.to be_valid }

    describe 'with an empty file' do
      before { File.truncate(path, 0) }

      it { is_expected.to be_valid }
    end
  end

  describe '#large?' do
    before do
      allow(FileTracker).to receive(:large_file_threshold).and_return(200)
    end

    describe 'when size is nil' do
      subject { described_class.new(path:) }

      it { is_expected.not_to be_large }
    end

    describe 'when size is not nil' do
      subject { described_class.new(path:, size:) }

      let(:path) { Tempfile.create('foo').path }

      before do
        File.binwrite(path, SecureRandom.random_bytes(size))
      end

      after { File.unlink(path) }

      describe 'when size < large file threshold' do
        let(:size) { 100 }

        it { is_expected.not_to be_large }
      end

      describe 'when size == large file threshold' do
        let(:size) { 200 }

        it { is_expected.to be_large }
      end

      describe 'when size > large file threshold' do
        let(:size) { 300 }

        it { is_expected.to be_large }
      end
    end
  end
end
