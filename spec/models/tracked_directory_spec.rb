require 'rails_helper'

RSpec.describe TrackedDirectory do
  describe 'validation' do
    subject { described_class.new(path:, title: SecureRandom.hex(4)) }

    let(:path) { Dir.mktmpdir }

    after { Dir.rmdir(path) if Dir.exist?(path) }

    describe 'existence violation' do
      before { Dir.rmdir(path) }

      it { is_expected.to be_invalid }
    end

    describe 'uniqueness violation' do
      before { described_class.create!(path:, title: SecureRandom.hex(4)) }

      it { is_expected.to be_invalid }
    end
  end

  describe 'normalization of path' do
    subject { described_class.new(path: "#{path}/") }

    let(:path) { Rails.root.join('spec/fixtures') }

    before { subject.valid? }

    its(:path) { is_expected.to eq path.to_s }
  end
end
