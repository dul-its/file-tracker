require 'spec_helper'

ENV['RAILS_ENV'] ||= 'test'

require 'rails/all'
require 'rspec/rails'

require File.expand_path('../config/environment', __dir__)

# Add additional requires below this line. Rails is not loaded until this point!

# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?

ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  config.fixture_paths = %W[ #{Rails.root}/spec/fixtures ]
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")
end
