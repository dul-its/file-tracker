require 'rails_helper'

RSpec.describe 'FileTracker API' do
  describe 'Swagger doc' do
    it 'is successful' do
      get '/api/swagger_doc.json'
      expect(response.status).to eq 200
    end
  end
end
