require 'rails_helper'

RSpec.describe TrackedFilesAPI, type: :request do
  let(:user) { User.create(uid: SecureRandom.uuid) }

  let(:headers) { { 'Content-Type' => 'application/json' } }

  before do
    login_as user
  end

  describe 'GET /tracked_files' do
    it 'is successful' do
      get '/api/tracked_files'
      expect(response.status).to eq 200
      expect(JSON.parse(response.body)).to be_a Array
    end
  end

  describe 'POST /tracked_files' do
    describe 'when the file is valid' do
      it 'creates the tracked file' do
        Dir.mktmpdir do |dir|
          TrackedDirectory.create!(path: dir, title: SecureRandom.hex(4))
          Tempfile.open('', dir, encoding: 'ascii-8bit') do |file|
            file.write SecureRandom.random_bytes(123)
            file.close
            params = JSON.dump(files: [{ path: file.path }])
            post('/api/tracked_files', params:, headers:)
            expect(response.status).to eq 201
          end
        end
      end
    end

    describe 'when the file is not valid' do
      it 'is a bad request' do
        params = JSON.dump(files: [{ path: '/foo/bar' }])
        post('/api/tracked_files', params:, headers:)
        expect(response.status).to eq 400
      end
    end
  end

  describe 'GET /tracked_files/:id' do
    it 'finds the tracked file' do
      Dir.mktmpdir do |dir|
        TrackedDirectory.create!(path: dir, title: SecureRandom.hex(4))
        Tempfile.open('', dir, encoding: 'ascii-8bit') do |file|
          file.write SecureRandom.random_bytes(123)
          tf = TrackedFile.create!(path: file.path)
          get '/api/tracked_files/%s' % tf.id
          expect(response.status).to eq 200
          expect(JSON.parse(response.body)['id']).to eq tf.id
        end
      end
    end

    it 'is not found when the file does not exist' do
      get '/api/tracked_files/12345'
      expect(response.status).to eq 404
    end
  end
end
