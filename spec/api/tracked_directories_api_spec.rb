require 'rails_helper'

RSpec.describe TrackedDirectoriesAPI, type: :request do
  let(:user) { User.create(uid: SecureRandom.uuid) }

  let(:headers) { { 'Accept' => 'application/json', 'Content-Type' => 'application/json' } }

  before do
    login_as user
  end

  describe 'GET /tracked_directories' do
    it 'is successful' do
      get '/api/tracked_directories'
      expect(response.status).to eq 200
    end
  end

  describe 'POST /tracked_directories' do
    describe 'when the directory is valid' do
      it 'creates the tracked directory' do
        Dir.mktmpdir do |path|
          params = JSON.dump(path:, title: SecureRandom.hex(4))
          post('/api/tracked_directories', params:, headers:)
          expect(response.status).to eq 201
          data = JSON.parse(response.body)
          expect(TrackedDirectory.find(data['id'])).to be_a TrackedDirectory
        end
      end
    end

    describe 'when the directory is not valid' do
      it 'is a bad request' do
        params = JSON.dump(path: '/foo/bar', title: SecureRandom.hex(4))
        post('/api/tracked_directories', params:, headers:)
        expect(response.status).to eq 400
      end
    end
  end

  describe 'POST /tracked_directories/inventory' do
    it 'is enqueues a job' do
      allow(TrackDirectoryBatchJob).to receive(:perform_later)

      Dir.mktmpdir do |path|
        TrackedDirectory.create!(path:, title: SecureRandom.hex(4))
        post '/api/tracked_directories/inventory'
        expect(response.status).to eq 202
      end

      expect(TrackDirectoryBatchJob).to have_received(:perform_later)
    end
  end

  describe 'GET /tracked_directories/:id' do
    describe 'when the directory exists' do
      it 'is successful' do
        Dir.mktmpdir do |path|
          dir = TrackedDirectory.create!(path:, title: SecureRandom.hex(4))
          get '/api/tracked_directories/%s' % dir.id
          expect(response.status).to eq 200
        end
      end
    end

    describe 'when directory does not exist' do
      it 'is not found' do
        get '/api/tracked_directories/12345'
        expect(response.status).to eq 404
      end
    end
  end

  describe 'GET /tracked_directories/:id/locate' do
    describe 'when the directory exists' do
      it 'returns the locate db stats' do
        Dir.mktmpdir do |path|
          dir = TrackedDirectory.create!(path:, title: SecureRandom.hex(4))
          get '/api/tracked_directories/%s/locate' % dir.id, headers: { 'Accept' => 'text/plain' }
          expect(response.status).to eq 200
        end
      end
    end

    describe 'when directory does not exist' do
      it 'is not found' do
        get '/api/tracked_directories/12345/locate'
        expect(response.status).to eq 404
      end
    end
  end

  describe 'POST /tracked_directories/:id/locate' do
    describe 'when the directory exists' do
      it 'is accepted' do
        Dir.mktmpdir do |path|
          dir = TrackedDirectory.create!(path:, title: SecureRandom.hex(4))
          post '/api/tracked_directories/%s/locate' % dir.id
          expect(response.status).to eq 202
        end
      end
    end

    describe 'when directory does not exist' do
      it 'is not found' do
        post '/api/tracked_directories/12345/locate'
        expect(response.status).to eq 404
      end
    end
  end

  describe 'POST /tracked_directories/:id/inventory' do
    describe 'when the directory exists' do
      it 'is enqueues a job' do
        allow(TrackDirectoryJob).to receive(:perform_later)

        Dir.mktmpdir do |path|
          dir = TrackedDirectory.create!(path:, title: SecureRandom.hex(4))
          post '/api/tracked_directories/%s/inventory' % dir.id
          expect(response.status).to eq 202
        end

        expect(TrackDirectoryJob).to have_received(:perform_later)
      end
    end

    describe 'when directory does not exist' do
      it 'is not found' do
        post '/api/tracked_directories/12345/inventory'
        expect(response.status).to eq 404
      end
    end
  end
end
