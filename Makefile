SHELL = /bin/bash

repository ?= file-tracker
build_tag ?= $(repository):latest

.PHONY: build
build:
	repository=$(repository) build_tag=$(build_tag) ./build.sh

.PHONY: clean
clean:
	rm -rf ./tmp/*

.PHONY: test
test: clean
	./.docker/test.sh up --exit-code-from app; \
	code=$$?; \
	./.docker/test.sh down; \
	exit $$code

.PHONY: lock
lock:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle lock

.PHONY: update
update:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle update $(args)

.PHONY: audit
audit:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) ./audit.sh

.PHONY: rubocop
rubocop:
	docker run --rm -v "$(shell pwd):/opt/app-root" $(build_tag) \
		bundle exec rubocop $(args)
