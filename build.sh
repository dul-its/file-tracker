#!/bin/bash

set -x

repository="${repository:-${CI_REGISTRY_IMAGE}}"

if [[ -z "${repository}" ]]; then
    echo "The 'repository' or 'CI_REGISTRY_IMAGE' variable is not set."
    exit 1
fi

# For older Docker versions
export DOCKER_BUILDKIT="1"

dockerfile_changeset=".ruby-version .dockerignore Dockerfile"
bundle_changeset="${dockerfile_changeset} Gemfile.lock"

# The builder should be updated whenever the Dockerfile changes
builder_md5="$(cat ${dockerfile_changeset} | md5sum | awk '{print $1}')"
builder_tag="builder-${builder_md5}"
builder="${repository}:${builder_tag}"

# The bundle should be updated whenever the Dockerfile or Gemfile.lock changes
bundle_md5="$(cat ${bundle_changeset} | md5sum | awk '{print $1}')"
bundle_tag="bundle-${bundle_md5}"
bundle="${repository}:${bundle_tag}"

git_commit="${CI_COMMIT_SHA:-$(git rev-parse HEAD)}"

ci_pipeline() {
    [[ -n "${CI}" ]]
}

#
# Image is:
# - not ready if force_build is set
# - ready if it is present locally
# - ready in a CI pipeline if it can be pulled
#
image_is_ready() {
    [[ -n "${force_build}" ]] && return 1
    [[ -n "$(docker images -q $1)" ]] && return 0
    ci_pipeline && docker pull -q $1
}

# Build the builder stage, if necessary
if ! image_is_ready $builder; then
    docker build --pull -t ${builder} --target builder \
	   --build-arg ruby_version=$(cat .ruby-version) .
    ci_pipeline && docker push $builder
fi

# Build the bundle stage, if necessary
if ! image_is_ready $bundle; then
    docker build -t ${bundle} --build-arg builder=${builder} --target bundle .
    ci_pipeline && docker push $bundle
fi

docker build -t ${build_tag} --target app \
       --build-arg APP_VERSION=$(cat ./VERSION) \
       --build-arg APP_VCS_REF=${CI_COMMIT_SHORT_SHA:-$(git rev-parse --short HEAD)} \
       --build-arg BUILD_DATE=$(date -Iseconds) \
       --build-arg bundle=${bundle} \
       .
