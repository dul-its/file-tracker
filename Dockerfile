ARG ruby_version="3.1.4"
ARG builder="builder"
ARG bundle="bundle"

FROM ruby:${ruby_version} AS builder

SHELL ["/bin/bash", "-c"]

ENV BUNDLE_IGNORE_CONFIG="true" \
    BUNDLE_USER_HOME="${GEM_HOME}" \
    APP_ROOT="/opt/app-root" \
    APP_USER="app-user" \
    APP_UID="1001" \
    APP_GID="0" \
    APP_VCS_REF="${APP_VCS_REF}" \
    APP_VERSION="${APP_VERSION}" \
    DATA_VOLUME="/data" \
    LANG="en_US.UTF-8" \
    LANGUAGE="en_US:en" \
    RAILS_ENV="production" \
    RAILS_PORT="3000" \
    TZ="US/Eastern"

RUN set -eux; \
    apt-get -y update; \
    apt-get -y install \
    default-jre \
    hashdeep \
    jq \
    libjemalloc2 \
    libpq-dev \
    nodejs \
    npm \
    plocate \
    wait-for-it \
    ; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
    \
    npm install -g yarn

WORKDIR $APP_ROOT

#-------------------------------+

FROM ${builder} AS bundle

COPY Gemfile Gemfile.lock .ruby-version ./

RUN set -eux; \
    gem install bundler -v "$(tail -1 Gemfile.lock | awk '{print $1}')"; \
    bundle install; \
    chmod -R g=u $GEM_HOME

#--------------------------------+

FROM ${bundle} AS app

ARG APP_VERSION="0.0.0"
ARG APP_VCS_REF="0"
ARG BUILD_DATE="1970-01-01T00:00:00Z"

LABEL org.opencontainers.artifact.description="FileTracker tracks files"
LABEL org.opencontainers.image.url="https://file-tracker.lib.duke.edu"
LABEL org.opencontainers.image.source="https://gitlab.oit.duke.edu/dul-its/file-tracker"
LABEL org.opencontainers.image.version="${APP_VERSION}"
LABEL org.opencontainers.image.revision="${APP_VCS_REF}"
LABEL org.opencontainers.image.vendor="Duke University Libraries"
LABEL org.opencontainers.image.license="BSD-3-Clause"

COPY . .

# N.B. We added app-user to gid 500 (dukeusers) for group access to RDR fcrepo binary files.
RUN set -eux; \
    groupadd -g 500 -f dukeusers; \
    useradd -r -u $APP_UID -g $APP_GID -G 500 -d $APP_ROOT -s /sbin/nologin $APP_USER; \
    mkdir -p -m 0775 $DATA_VOLUME; \
    chown -R $APP_UID:$APP_GID $DATA_VOLUME .; \
    git config --system --add safe.directory "*"

USER $APP_USER

RUN SECRET_KEY_BASE=1 ./bin/rails assets:precompile

VOLUME $DATA_VOLUME

EXPOSE $RAILS_PORT

CMD ["./bin/rails", "server"]
